/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import route from 'ziggy';
import { Ziggy } from './ziggy';
import Vuex from 'vuex';

require('./bootstrap');

window.Vue = require('vue');

Vue.mixin({
    methods: {
        route: (name, params, absolute) => route(name, params, absolute, Ziggy),
    },
});

const store = new Vuex.Store({
    state: {
        csrf: document.querySelector('meta[name="csrf-token"]').getAttribute('content')
    }
});

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('universe-edit-component', require('./components/UniverseEditComponent.vue').default);
Vue.component('series-edit-component', require('./components/SeriesEditComponent.vue').default);
Vue.component('team-edit-component', require('./components/TeamEditComponent.vue').default);
Vue.component('universe-index-component', require('./components/UniverseIndexComponent.vue').default);
Vue.component('series-index-component', require('./components/SeriesIndexComponent').default);
Vue.component('season-edit-component', require('./components/SeasonEditComponent').default);
Vue.component('season-index-component', require('./components/SeasonIndexComponent').default);
Vue.component('race-edit-component', require('./components/RaceEditComponent').default);
Vue.component('points-system-component', require('./components/PointSystemComponent').default);
Vue.component('reliability-edit-component', require('./components/ReliabilityEditComponent').default);
Vue.component('season-add-team-component', require('./components/SeasonAddTeamComponent').default);
Vue.component('qualifying-settings-component', require('./components/QualifyingSettingsComponent').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    store
});
