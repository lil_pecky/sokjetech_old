@extends('layouts.sim')

@section('content')
<div class="container">
    {{ Breadcrumbs::render('universes.show', $universe)}}

    <h1>{{ $universe->name }}</h1>

    <universe-index-component :universe="{{ $universe }}" :can-edit="{{ json_encode($canEdit) }}" :tab="{{ json_encode($tab) }}">
    </universe-index-component>
</div>
@endsection
