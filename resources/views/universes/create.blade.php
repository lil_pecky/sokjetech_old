@extends('layouts.sim')

@section('content')
    <div class="container">
        {{ Breadcrumbs::render('universes.create') }}

        <div class="card">
            <div class="card-header">
                <h2>Create universe</h2>
            </div>

            <div class="card-body">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <p>{{ $error }}</p>
                        @endforeach
                    </div>
                @endif
                <universe-edit-component></universe-edit-component>
            </div>
        </div>
    </div>
@endsection
