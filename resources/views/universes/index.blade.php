@extends('layouts.sim')

@section('content')
    <div class="container">

        {{ Breadcrumbs::render('universes') }}

        <table class="table">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Owner</th>
                    <th>Name</th>
                    <th>Slug</th>
                    <th>Public</th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($universes as $universe)
                    <tr>
                        <td>{{ $universe->id }}</td>
                        <td>{{ $universe->user->username }}</td>
                        <td>{{ $universe->name }}</td>
                        <td>{{ $universe->slug }}</td>
                        <td>{{ $universe->public ? 'Yes' : 'No' }}</td>
                        <td><a href="{{ route('universes.show', $universe) }}">View</a></td>
                        <td>
                            @if (Auth::check() && Auth::user()->id === $universe->user_id)
                                <a href="{{ route('universes.edit', $universe) }}">Edit</a>
                            @endif
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection
