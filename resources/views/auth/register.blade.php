@extends('layouts.main')

@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">
                <h2>Register</h2>
            </div>
            <div class="card-body">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <p>{{ $error }}</p>
                        @endforeach
                    </div>
                @endif
                <form action="{{ route('register') }}" method="POST">
                    @csrf

                    <div class="form-group">
                        <label for="username">Username *</label>
                        <input type="text" id="username" name="username" class="form-control" value="{{ old('username') }}" placeholder="Username" required>
                    </div>

                    <div class="form-group">
                        <label for="email">Email *</label>
                        <input type="email" id="email" name="email" class="form-control" value="{{ old('email') }}" placeholder="Email" required>
                    </div>

                    <div class="form-group">
                        <label for="password">Password *</label>
                        <input type="password" id="password" name="password" class="form-control" value="{{ old('password') }}" placeholder="Password" required>
                    </div>

                    <div class="form-group">
                        <label for="password-confirm">Confirm password *</label>
                        <input type="password" id="password-confirm" name="password_confirmation" class="form-control" value="{{ old('password-confirm') }}" placeholder="Confirm password" required>
                    </div>

                    <div class="form-group">
                        <label for="discord">Discord username</label>
                        <input type="text" id="discord" name="discord" class="form-control" value="{{ old('discord') }}" placeholder="Discord username">
                    </div>

                    <input type="submit" class="btn btn-secondary" value="Register">
                </form>
            </div>
        </div>
    </div>
@endsection
