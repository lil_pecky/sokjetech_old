@extends('layouts.sim')

@section('content')
    <div class="container">
        {{ Breadcrumbs::render('engines.edit', $engine) }}

        <div class="card">
            <div class="card-header">
                <h2>Edit {{ $engine->name }}</h2>
            </div>
            <div class="card-body">
                <form action="{{ route('engines.update', [$engine->series->universe, $engine->series, $engine]) }}" method="POST">
                    @csrf
                    @method('PATCH')

                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" id="name" name="name" class="form-control" value="{{ $engine->name }}" placeholder="Name" required>
                    </div>

                    <button type="submit" class="btn btn-secondary">Save</button>
                </form>
            </div>
        </div>
    </div>
@endsection
