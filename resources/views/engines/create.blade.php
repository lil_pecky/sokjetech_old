@extends('layouts.sim')

@section('content')
    <div class="container">
        {{ Breadcrumbs::render('engines.create', $series) }}

        <div class="card">
            <div class="card-header">
                <h2>Create engine</h2>
            </div>
            <div class="card-body">
                <form action="{{ route('engines.store', [$series->universe, $series]) }}" method="POST">
                    @csrf

                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" id="name" name="name" class="form-control" placeholder="Name" required>
                    </div>

                    <button type="submit" class="btn btn-secondary">Save</button>
                </form>
            </div>
        </div>
    </div>
@endsection
