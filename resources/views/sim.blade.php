@extends('layouts.sim')

@section('content')
<div class="container">

    {{ Breadcrumbs::render('home') }}

    <h2>Simulation zone</h2>

    <div class="items">
        <div class="item-card">
            <div class="item-card-header">
                <h2>Universes</h2>
            </div>

            <div class="item-card-body">
                <p>Universe related shite</p>
            </div>

            <div class="item-card-footer">
                <a href="{{ route('universes.index') }}" class="btn btn-primary">Overview</a>
                <a href="{{ route('universes.create') }}" class="btn btn-secondary">New</a>
            </div>
        </div>
    </div>
</div>
@endsection
