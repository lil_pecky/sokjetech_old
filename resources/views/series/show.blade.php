@extends('layouts.sim')

@section('content')
    <div class="container">
        {{ Breadcrumbs::render('series.show', $series)}}

        <h2>{{ $series->name }}</h2>

        <series-index-component :series="{{ $series }}"
                                :can-edit="{{ json_encode($canEdit) }}" :tab="{{ json_encode($tab) }}"
                                :can-see="{{ json_encode($canSee) }}">
        </series-index-component>
    </div>
@endsection
