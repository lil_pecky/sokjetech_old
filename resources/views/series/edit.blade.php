@extends('layouts.sim')

@section('content')
<div class="container">
    {{ Breadcrumbs::render('series.edit', $series) }}

    <div class="card">
        <div class="card-header">
            <h2>Edit {{ $series->name }}</h2>
        </div>
        <div class="card-body">
            @if ($errors->any())
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                <p>{{ $error }}</p>
                @endforeach
            </div>
            @endif
            <series-edit-component :series="{{ $series }}"></series-edit-component>
        </div>
    </div>
</div>
@endsection
