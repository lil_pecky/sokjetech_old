@extends('layouts.sim')

@section('content')
    <div class="container">
        {{ Breadcrumbs::render('series', $series) }}

        <h2>{{ $series->universe->name }} - {{ $series->name }}</h2>


    </div>
@endsection
