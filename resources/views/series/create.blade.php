@extends('layouts.sim')

@section('content')
    <div class="container">
        {{ Breadcrumbs::render('series.create', $universe)}}

        <div class="card">
            <div class="card-header">
                <h2>Create series</h2>
            </div>
            <div class="card-body">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <p>{{ $error }}</p>
                        @endforeach
                    </div>
                @endif
            </div>
            <series-edit-component :universe="{{ $universe }}"></series-edit-component>
        </div>
    </div>
@endsection
