@extends('layouts.sim')

@section('content')
    <div class="container">
        {{ Breadcrumbs::render('teams.edit', $team) }}

        <div class="card">
            <div class="card-header">
                <h2>Edit {{ $team->name }}</h2>
            </div>
            <div class="card-body">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <p>{{ $error }}</p>
                        @endforeach
                    </div>
                @endif
                <team-edit-component :series="{{ $team->series }}" :team="{{ $team }}">
                </team-edit-component>
            </div>
        </div>
    </div>
@endsection
