@extends('layouts.sim')

@section('content')
    <div class="container">
        {{ Breadcrumbs::render('teams.create', $series) }}

        <div class="card">
            <div class="card-header">
                <h2>Create team</h2>
            </div>
            <div class="card-body">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <p>{{ $error }}</p>
                        @endforeach
                    </div>
                @endif
                <team-edit-component :series="{{ $series }}"></team-edit-component>
            </div>
        </div>
    </div>
@endsection
