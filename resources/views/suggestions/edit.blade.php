@extends('layouts.main')

@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">
                <h2>Edit suggestion {{ $suggestion->title }}</h2>
            </div>
            <div class="card-body">
                @if ($errors->any())
                    @foreach ($errors->all() as $error)
                        <p>{{ $error }}</p>
                    @endforeach
                @endif

                <form action="{{ route('suggestions.update', $suggestion) }}" method="POST">
                    @csrf
                    @method('PATCH')

                    <div class="form-group">
                        <label for="title">Title</label>
                        <input type="text" id="title" name="title" class="form-control" value="{{ old('title') ?? $suggestion->title }}" placeholder="Title" required>
                    </div>

                    <div class="form-group">
                        <label for="description">Description</label>
                        <textarea name="description" id="description" cols="30" rows="10" class="form-control" placeholder="Description" required>{{ old('description') ?? $suggestion->description }}</textarea>
                    </div>

                    <div class="form-group">
                        <label for="category_id">Category</label>
                        <select name="category_id" id="category_id" class="form-control" required>
                            @foreach ($categories as $category)
                                <option value="{{ $category->id }}" {{ $category->id === $suggestion->category->id ? 'selected' : '' }}>{{ ucfirst($category->name) }}</option>
                            @endforeach
                        </select>
                    </div>

                    <button type="submit" class="btn btn-secondary">Update</button>
                </form>
            </div>
        </div>
    </div>
@endsection
