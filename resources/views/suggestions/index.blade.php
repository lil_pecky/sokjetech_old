@extends('layouts.main')

@section('content')
<div class="container">
    <h2>Suggestions</h2>

    @auth
    <a href="{{ route('suggestions.create') }}" class="btn btn-primary btn-small">Submit suggestion</a>
    @endauth
    <table>
        <thead>
            <tr>
                <th>#</th>
                <th>Title</th>
                <th>Category</th>
                <th>Status</th>
                <th></th>
                @if (Auth::check() && Auth::user()->isAdmin())
                <th></th>
                @endif
            </tr>
        </thead>
        <tbody>
            @foreach ($suggestions as $suggestion)
            <tr>
                <td>{{ $suggestion->id }}</td>
                <td>{{ $suggestion->title }}</td>
                <td>
                    <span class="label" style="background-color: {{$suggestion->category->colour}}">
                        {{ ucfirst($suggestion->category->name) }}
                    </span>
                </td>
                <td>
                    <span class="label" style="background-color: {{$suggestion->status->colour}}">
                        {{ ucfirst($suggestion->status->name) }}
                    </span>
                </td>
                <td>
                    @if (Auth::check() && (Auth::user()->isAdmin() || Auth::user()->id === $suggestion->user->id))
                    <a href="{{ route('suggestions.edit', $suggestion) }}" class="btn btn-secondary btn-small">
                        Edit
                    </a>
                    @endif
                </td>
                @if (Auth::check() && Auth::user()->isAdmin())
                <td>
                    <form action="{{ route('suggestions.status', $suggestion) }}" method="POST">
                        @csrf
                        @method('PATCH')

                        @if ($suggestion->status->name !== 'accepted')
                        <button type="submit" name="action" value="accept"
                            class="btn btn-success btn-small">Accept</button>
                        @endif
                        @if ($suggestion->status->name !== 'rejected')
                        <button type="submit" name="action" value="reject"
                            class="btn btn-danger btn-small">Reject</button>
                        @endif
                    </form>
                </td>
                @endif
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection
