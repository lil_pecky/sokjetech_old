@extends('layouts.main')

@section('content')
<div class="container">
    <div class="card">
        <div class="card-header">
            <h2>New suggestion</h2>
        </div>

        <div class="card-body">
            @if ($errors->any())
            @foreach ($errors->all() as $error)
            <p>{{ $error }}</p>
            @endforeach
            @endif
        </div>

        <form action="{{ route('suggestions.store') }}" method="POST">
            @csrf

            <div class="form-group">
                <label for="title">Title</label>
                <input type="text" id="title" name="title" class="form-control" value="{{ old('title') }}"
                    placeholder="Title" required>
            </div>

            <div class="form-group">
                <label for="description">Description</label>
                <textarea name="description" id="description" cols="30" rows="10" class="form-control"
                    placeholder="Description" required>{{ old('description') }}</textarea>
            </div>

            <div class="form-group">
                <label for="category_id">Category</label>
                <select name="category_id" id="category_id" class="form-control" required>
                    @foreach ($categories as $category)
                    <option value="{{ $category->id }}">{{ ucfirst($category->name) }}</option>
                    @endforeach
                </select>
            </div>

            <button type="submit" class="btn btn-secondary">Save suggestion</button>
        </form>
    </div>
</div>
@endsection
