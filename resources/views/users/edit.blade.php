@extends('layouts.main')

@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">
                <h2>Edit profile</h2>
            </div>

            <div class="card-body">
                @if ($errors->any())
                    @foreach ($errors->all() as $error)
                        <p>{{ $error }}</p>
                    @endforeach
                @endif
                <form action="{{ route('users.update', $user) }}" method="POST">
                    @csrf
                    @method('PATCH')

                    <div class="form-group">
                        <label for="username">Username</label>
                        <input type="text" id="username" name="username" class="form-control"  value="{{ $user->username }}" placeholder="Username" required>
                    </div>

                    <div class="form-group">
                        <label for="email">Email address</label>
                        <input type="email" id="email" name="email" class="form-control" value="{{ $user->email }}" placeholder="Email address" required>
                    </div>

                    <div class="form-group">
                        <label for="discord">Discord username</label>
                        <input type="text" id="discord" name="discord" class="form-control" value="{{ $user->discord }}" placeholder="Discord username">
                    </div>

                    <button type="submit" class="btn btn-secondary">Update</button>
                </form>
            </div>
        </div>
    </div>
@endsection
