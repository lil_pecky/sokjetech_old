@extends('layouts.main')

@section('content')
    <div class="container">
        <h2>Welcome {{ $user->username }}</h2>
        <a href="{{ route('users.edit', $user) }}" class="btn btn-secondary btn-small">Edit profile</a>
        <a href="{{ route('suggestions.create') }}" class="btn btn-primary btn-small">Submit suggestion</a>

        <p>Username: {{ $user->username }}</p>
        <p>Email address: {{ $user->email }}</p>
        <p>Discord name: {{ $user->discord }}</p>
    </div>
@endsection
