<header>
    {{-- <a href="{{ route('index') }}" class="logo">{{ config('app.name', 'Laravel') }}</a> --}}
    <nav class="menu">
        <ul>
            <li><a href="{{ route('index') }}">Home</a></li>
            <li><a href="{{ route('sim.index') }}">Simulation Zone</a></li>
        </ul>
    </nav>
    <div class="cta">
        <ul>
            @auth
            <li><a href="{{ route('users.show', Auth::user()) }}">Profile</a></li>
            <li><a href="{{ route('suggestions.index') }}">Suggestions</a></li>
            <li><a href="{{ route('logout') }}" onclick="event.preventDefault();
                document.getElementById('logout-form').submit();">
                    {{ __('Logout') }}
                </a>
            </li>
            @if (Auth::user()->isAdmin())
            <li><a href="#" class="btn btn-secondary">Admin</a></li>
            @endif
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
            @else
            <li><a href="{{ route('login') }}">Login</a></li>
            <li><a href="{{ route('register') }}">Register</a></li>
            @endauth
        </ul>
    </div>
</header>
