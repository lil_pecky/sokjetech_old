<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel')}}</title>

    <script src="{{ asset('js/app.js') }}" defer></script>

    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
</head>

<body>
    <div id="app">
        @include('includes.menu-main')

        <main class="py-4">
            @if (Session::has('message'))
                <div class="alert alert-small alert-info">
                    {{ Session::get('message') }}
                </div>
            @endif

            @yield('content')
        </main>
    </div>
</body>

</html>
