@extends('layouts.sim')

@section('content')
    <div class="container">
        {{ Breadcrumbs::render('drivers.edit', $driver) }}

        <div class="card">
            <div class="card-header">
                <h2>Edit {{ $driver->getFullName() }}</h2>
            </div>
            <div class="card-body">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <p>{{ $error }}</p>
                        @endforeach
                    </div>
                @endif
                <form action="{{ route('drivers.update', [$driver->universe, $driver]) }}" method="POST">
                    @csrf
                    @method('PATCH')

                    <div class="form-group">
                        <label for="first_name">First name</label>
                        <input type="text" id="first_name" name="first_name" class="form-control" value="{{ old('first_name') ?? $driver->first_name }}" placeholder="First name" required>
                    </div>

                    <div class="form-group">
                        <label for="last_name">Last name</label>
                        <input type="text" id="last_name" name="last_name" class="form-control" value="{{ old('last_name') ?? $driver->last_name }}" placeholder="Last name" required>
                    </div>

                    <div class="form-group">
                        <label for="birthdate">Birthdate</label>
                        <input type="date" id="birthdate" name="birthdate" class="form-control" value="{{ old('birthdate') ?? $driver->birthdate }}" required>
                    </div>

                    <button type="submit" class="btn btn-secondary">Save</button>
                </form>
            </div>
        </div>
    </div>
@endsection
