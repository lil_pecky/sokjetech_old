@extends('layouts.sim')

@section('content')
    <div class="container">
        {{ Breadcrumbs::render('seasons.edit', $season) }}

        <div class="card">
            <div class="card-header">
                <h2>Edit {{ $season->name }}</h2>
            </div>
            <div class="card-body">
                @if ($errors->any())
                    @foreach ($errors->all() as $error)
                        <p>{{ $error }}</p>
                    @endforeach
                @endif
                <season-edit-component :season="{{ $season }}"></season-edit-component>
            </div>
        </div>
    </div>
@endsection
