@extends('layouts.sim')

@section('content')
    <div class="container">
        {{ Breadcrumbs::render('seasons.qualifying', $season) }}

        <qualifying-settings-component :season="{{ $season }}"></qualifying-settings-component>
    </div>
@endsection