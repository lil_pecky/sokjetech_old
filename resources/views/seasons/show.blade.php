@extends('layouts.sim')

@section('content')
    <div class="container">
        {{ Breadcrumbs::render('seasons.show', $season) }}

        <h2>{{ $season->name }}</h2>

        <season-index-component :season="{{ $season }}" :tab="{{ json_encode($tab) }}"
                                :can-edit="{{ json_encode($canEdit) }}">
        </season-index-component>
    </div>
@endsection
