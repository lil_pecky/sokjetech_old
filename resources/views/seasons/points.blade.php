@extends('layouts.sim')

@section('content')
    <div class="container">
        {{ Breadcrumbs::render('seasons.points', $season) }}

        <div class="card">
            <div class="card-header">
                <h2>Configure points for {{ $season->name }}</h2>
            </div>
            <div class="card-body">
                @if ($errors->any())
                    @foreach ($errors->all() as $error)
                        <p>{{ $error }}</p>
                    @endforeach
                @endif
                <points-system-component :season="{{ $season }}">
                </points-system-component>
            </div>
        </div>
    </div>
@endsection
