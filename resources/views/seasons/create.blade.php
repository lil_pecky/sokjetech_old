@extends('layouts.sim')

@section('content')
    <div class="container">
        {{ Breadcrumbs::render('seasons.create', $series) }}

        <div class="card">
            <div class="card-header">
                <h2>Create season</h2>
            </div>
            <div class="card-body">
                @if ($errors->any())
                    @foreach ($errors->all() as $error)
                        <p>{{ $error }}</p>
                    @endforeach
                @endif
                <season-edit-component :series="{{ $series }}"></season-edit-component>
            </div>
        </div>
    </div>
@endsection
