@extends('layouts.sim')

@section('content')
    <div class="container">
        {{ Breadcrumbs::render('seasons.reliability', $season) }}

        <div class="card">
            <div class="card-header">
                <h2>Configure reliability</h2>
            </div>
            <div class="card-body">
                @if ($errors->any())
                    @foreach ($errors->all() as $error)
                        <p>{{ $error }}</p>
                    @endforeach
                @endif
                <reliability-edit-component :season="{{ $season }}"></reliability-edit-component>
            </div>
        </div>
    </div>
@endsection
