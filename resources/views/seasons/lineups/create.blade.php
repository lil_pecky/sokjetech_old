@extends('layouts.sim')

@section('content')
        <div class="container">
            {{ Breadcrumbs::render('seasons.lineups.create', $season) }}
        </div>

        <season-add-team-component :season="{{ $season }}"></season-add-team-component>
    </div>
@endsection
