@extends('layouts.sim')

@section('content')
    <div class="container">
        {{ Breadcrumbs::render('seasons.lineups.edit', $season, $lineup) }}
    </div>

    <season-add-team-component :season="{{ $season }}" :lineup="{{ $lineup->id }}"></season-add-team-component>
@endsection