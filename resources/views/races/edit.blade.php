@extends('layouts.sim')

@section('content')
    <div class="container">
        {{ Breadcrumbs::render('races.edit', $race) }}

        <div class="card">
            <div class="card-header">
                <h2>Edit {{ $race->name }}</h2>
            </div>
            @if ($errors->any())
                @foreach ($errors->all() as $error)
                    <p>{{ $error }}</p>
                @endforeach
            @endif
            <race-edit-component :season="{{ $race->season }}" :race="{{ $race }}" :can-edit="{{ json_encode($canEdit) }}"></race-edit-component>
        </div>
    </div>
@endsection

@section('styles')
    <style>
        .card {
            width: 70%;
            max-width: 1024px;
        }
    </style>
@endsection
