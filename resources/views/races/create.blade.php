@extends('layouts.sim')

@section('content')
    <div class="container">
        {{ Breadcrumbs::render('races.create', $season) }}

        <div class="card card-wide">
            <div class="card-header">
                <h2>Create race</h2>
            </div>
            <div class="card-body">
                @if ($errors->any())
                    @foreach ($errors->all() as $error)
                        <p>{{ $error }}</p>
                    @endforeach
                @endif
                <race-edit-component :season="{{ $season }}" :can-edit="{{ json_encode($canEdit) }}">
                </race-edit-component>
            </div>
        </div>
    </div>
@endsection
