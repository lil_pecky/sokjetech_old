<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SuggestionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            'users'     => '#E50000',
            'universes' => '#F9D900',
            'drivers'   => '#2ECD6F',
            'teams'     => '#04A9F4',
            'series'    => '#FF7800',
            'seasons'   => '#0231E8',
            'races'     => '#7C4DFF',
            'UI/UX'     => '#FF7FAB',
            'other'     => '#000000'
        ];

        $statuses = [
            'new'       => '#F47F0E',
            'accepted'  => '#025F2B',
            'rejected'  => '#BF1103'
        ];

        foreach ($categories as $name => $colour) {
            DB::table('suggestion_categories')->insert([
                'name'      => $name,
                'colour'    => $colour
            ]);
        }

        foreach ($statuses as $name => $colour) {
            DB::table('suggestion_statuses')->insert([
                'name'      => $name,
                'colour'    => $colour
            ]);
        }
    }
}
