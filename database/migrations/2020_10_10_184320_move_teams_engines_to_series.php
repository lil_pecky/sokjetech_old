<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MoveTeamsEnginesToSeries extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('teams', function (Blueprint $table) {
            $table->dropForeign('teams_universe_id_foreign');
            $table->dropColumn('universe_id');

            $table->unsignedBigInteger('series_id')->nullable();

            $table->foreign('series_id')->references('id')->on('series');
        });

        Schema::table('engines', function (Blueprint $table) {
            $table->dropForeign('engines_universe_id_foreign');
            $table->dropColumn('universe_id');

            $table->unsignedBigInteger('series_id')->nullable();

            $table->foreign('series_id')->references('id')->on('series');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('teams', function (Blueprint $table) {
            $table->dropForeign('teams_series_id_foreign');
            $table->dropColumn('series_id');

            $table->unsignedBigInteger('universe_id')->nullable();

            $table->foreign('universe_id')->references('id')->on('universes');
        });

        Schema::table('engines', function (Blueprint $table) {
            $table->dropForeign('engines_series_id_foreign');
            $table->dropColumn('series_id');

            $table->unsignedBigInteger('universe_id')->nullable();

            $table->foreign('universe_id')->references('id')->on('universes');
        });
    }
}
