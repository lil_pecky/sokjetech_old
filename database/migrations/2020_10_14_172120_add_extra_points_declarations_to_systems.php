<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddExtraPointsDeclarationsToSystems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('point_systems', function (Blueprint $table) {
            $table->unsignedBigInteger('pole_points')->default(0);
            $table->unsignedBigInteger('fastest_lap_points')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('point_systems', function (Blueprint $table) {
            $table->dropColumn('pole_points');
            $table->dropColumn('fastest_lap_points');
        });
    }
}
