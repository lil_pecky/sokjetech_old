<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDriverTeamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('driver_teams', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('team_id')->nullable();
            $table->unsignedBigInteger('driver_id')->nullable();
            $table->unsignedBigInteger('season_id')->nullable();
            $table->unsignedInteger('number');
            $table->unsignedInteger('rating')->nullable();
            $table->unsignedInteger('reliability')->nullable();
            $table->timestamps();

            $table->foreign('team_id')->references('id')->on('season_teams');
            $table->foreign('driver_id')->references('id')->on('drivers');
            $table->foreign('season_id')->references('id')->on('seasons');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('driver_teams');
    }
}
