<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEngineTeamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('engine_teams', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('team_id')->nullable();
            $table->unsignedBigInteger('engine_id')->nullable();
            $table->unsignedBigInteger('season_id')->nullable();
            $table->unsignedInteger('rating')->nullable();
            $table->unsignedInteger('reliability')->nullable();
            $table->boolean('rebadge')->nullable();
            $table->string('name')->nullable();
            $table->timestamps();

            $table->foreign('team_id')->references('id')->on('season_teams');
            $table->foreign('engine_id')->references('id')->on('engines');
            $table->foreign('season_id')->references('id')->on('seasons');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('engine_teams');
    }
}
