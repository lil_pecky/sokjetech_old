<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('index');

Route::prefix('sim')->group(function () {
    Route::get('', 'HomeController@sim')->name('sim.index');
});

Route::resource('users', 'UserController')->except([
    'index',
    'store',
    'create',
    'update'
]);
Route::patch('users/{user}', 'UserController@update')->name('users.update')->middleware('can_edit');

Route::resource('suggestions', 'SuggestionController');
Route::patch('suggestions/{suggestion}/status', 'SuggestionController@status')->name('suggestions.status');

Route::resource('universes', 'UniverseController')->except([
    'edit',
    'update'
]);

// TODO move to route groups and figure out why it ain't working
Route::middleware('can_edit_universe')->group(function () {
    Route::get('universes/{universe}/edit', 'UniverseController@edit')->name('universes.edit');
    Route::patch('universes/{universe}', 'UniverseController@update')->name('universes.update');
});

Route::post('universes/duplicate_slug', 'UniverseController@duplicateSlug');
Route::post('series/duplicate_slug', 'SeriesController@duplicateSlug');

// TODO fix your fucking routing 'cause it's becoming a mess
Route::prefix('universes')->group(function () {
    Route::prefix('{universe}')->group(function () {
        Route::delete('{series}/{season}/races/{race}', 'RaceController@destroy')->name('races.delete');

        Route::middleware('can_edit_universe')->group(function () {
            // Series
            Route::get('series/create', 'SeriesController@create')->name('series.create');
            Route::post('series', 'SeriesController@store')->name('series.store');
            Route::get('series/{series}/edit', 'SeriesController@edit')->name('series.edit');
            Route::patch('series/{series}', 'SeriesController@update')->name('series.update');

            // Teams
            Route::get('{series}/teams/create', 'TeamController@create')->name('teams.create');
            Route::get('{series}/teams/{team}/edit', 'TeamController@edit')->name('teams.edit');
            Route::post('{series}/teams', 'TeamController@store')->name('teams.store');
            Route::post('{series}/teams/{team}', 'TeamController@update')->name('teams.update');

            // Drivers
            Route::get('drivers/create', 'DriverController@create')->name('drivers.create');
            Route::post('drivers', 'DriverController@store')->name('drivers.store');
            Route::get('drivers/{driver}/edit', 'DriverController@edit')->name('drivers.edit');
            Route::patch('driver/{driver}', 'DriverController@update')->name('drivers.update');

            // Engines
            Route::get('{series}/engines/create', 'EngineController@create')->name('engines.create');
            Route::post('{series}/engine', 'EngineController@store')->name('engines.store');
            Route::get('{series}/engines/{engine}/edit', 'EngineController@edit')->name('engines.edit');
            Route::patch('{series}/engine/{engine}', 'EngineController@update')->name('engines.update');

            // Seasons
            Route::get('{series}/seasons/create', 'SeasonController@create')->name('seasons.create');
            Route::post('{series}/seasons', 'SeasonController@store')->name('seasons.store');
            Route::get('{series}/seasons/{season}/edit', 'SeasonController@edit')->name('seasons.edit');
            Route::patch('{series}/seasons/{season}', 'SeasonController@update')->name('seasons.update');

            // Races
            Route::get('{series}/seasons/{season}/races/create', 'RaceController@create')->name('races.create');
            Route::post('{series}/{season}/races', 'RaceController@store')->name('races.store');
            Route::get('{series}/seasons/{season}/races/{race}/edit', 'RaceController@edit')->name('races.edit');
            Route::patch('{series}/{season}/races/{race}', 'RaceController@update')->name('races.update');

            // Points
            Route::get('{series}/{season}/points', 'SeasonController@points')->name('seasons.points');
            Route::post('{series}/seasons/{season}/points', 'SeasonController@storePoints')->name('seasons.points.store');

            // Reliability
            Route::get('{series}/{season}/reliability', 'SeasonController@reliability')->name('seasons.reliability');
            Route::post('{series}/{season}/reliability', 'SeasonController@storeReliability')->name('seasons.reliability.store');

            // Qualifying settings
            Route::get('{series}/{season}/qualifying', 'SeasonController@qualifying')->name('seasons.qualifying');
            Route::post('{series}/{season}/qualifying', 'SeasonController@storeQualifying')->name('seasons.qualifying.store');

            // Lineups
            Route::get('{series}/{season}/lineup', 'LineupController@create')->name('seasons.lineups.create');
            Route::get('{series}/{season}/lineups/{lineup}/edit', 'LineupController@edit')->name('seasons.lineups.edit');

            Route::post('{series}/{season}/start', 'SeasonController@start')->name('seasons.start');
        });

        Route::middleware('can_see_universe')->group(function () {
            // Series
            Route::get('{series}', 'SeriesController@index')->name('series.index');
            Route::get('series/{series}', 'SeriesController@show')->name('series.show');

            // Seasons
            Route::get('{series}/season/{season}/{tab?}', 'SeasonController@show')->name('seasons.show');
        });
    });
});

Auth::routes();
