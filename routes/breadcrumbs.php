<?php

use App\Models\Driver;
use App\Models\Engine;
use App\Models\Race;
use App\Models\Season;
use App\Models\SeasonTeam;
use App\Models\Series;
use App\Models\Universe;
use App\Models\Team;
use DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs;

Breadcrumbs::for('home', function ($trail) {
    $trail->push('Home', route('sim.index'));
});

Breadcrumbs::for('universes', function ($trail) {
    $trail->parent('home');
    $trail->push('Universes', route('universes.index'));
});

Breadcrumbs::for('universes.show', function ($trail, Universe $universe) {
    $trail->parent('universes');
    $trail->push("{$universe->name}", route('universes.show', $universe));
});

Breadcrumbs::for('universes.create', function ($trail) {
    $trail->parent('universes');
    $trail->push('Create', route('universes.create'));
});

Breadcrumbs::for('universes.edit', function ($trail, Universe $universe) {
    $trail->parent('universes');
    $trail->push("Edit {$universe->name}", route('universes.edit', $universe));
});

Breadcrumbs::for('drivers.create', function ($trail, Universe $universe) {
    $trail->parent('universes.show', $universe);
    $trail->push("Create driver", route('drivers.create', $universe));
});

Breadcrumbs::for('drivers.edit', function ($trail, Driver $driver) {
    $universe = $driver->universe;
    $trail->parent('universes.show', $universe);
    $trail->push("Edit {$driver->getFullName()}", route('drivers.edit', [$universe, $driver]));
});

Breadcrumbs::for('series', function ($trail, Series $series) {
    $universe = $series->universe();
    $trail->parent('universes.show', $universe);
    $trail->push("{$series->name}", route('series.index', [$universe, $series]));
});

Breadcrumbs::for('series.create', function ($trail, Universe $universe) {
    $trail->parent('universes.show', $universe);
    $trail->push('Create Series', route('series.create', $universe));
});

Breadcrumbs::for('series.edit', function ($trail, Series $series) {
    $universe = $series->universe;
    $trail->parent('universes.show', $universe);
    $trail->push("Edit {$series->name}", route('series.edit', [$universe, $series]));
});

Breadcrumbs::for('series.show', function ($trail, Series $series) {
    $universe = $series->universe;
    $trail->parent('universes.show', $universe);
    $trail->push("{$series->name}", route('series.show', [$universe, $series]));
});

Breadcrumbs::for('teams.create', function ($trail, Series $series) {
    $trail->parent('series.show', $series);
    $trail->push("Create team", route('teams.create', [$series->universe, $series]));
});

Breadcrumbs::for('teams.edit', function ($trail, Team $team) {
    $series = $team->series;
    $trail->parent('series.show', $series);
    $trail->push("Edit {$team->name}", route('teams.edit', [$series->universe, $series, $team]));
});

Breadcrumbs::for('engines.create', function ($trail, Series $series) {
    $trail->parent('series.show', $series);
    $trail->push("Create engine", route('engines.create', [$series->universe, $series]));
});

Breadcrumbs::for('engines.edit', function ($trail, Engine $engine) {
    $series = $engine->series;
    $trail->parent('series.show', $series);
    $trail->push("Edit {$engine->name}", route('engines.edit', [$series->universe, $series, $engine]));
});

Breadcrumbs::for('seasons.create', function ($trail, Series $series) {
    $trail->parent('series.show', $series);
    $trail->push("Create season", route('seasons.create', [$series->universe, $series]));
});

Breadcrumbs::for('seasons.edit', function ($trail, Season $season) {
    $series = $season->series;
    $trail->parent('series.show', $series);
    $trail->push("Edit {$season->year}", route('seasons.edit', [$series->universe, $series, $season]));
});

Breadcrumbs::for('seasons.show', function ($trail, Season $season) {
    $series = $season->series;
    $trail->parent('series.show', $series);
    $trail->push("{$season->name}", route('seasons.show', [$series->universe, $series, $season]));
});

Breadcrumbs::for('races.create', function ($trail, Season $season) {
    $series = $season->series;
    $trail->parent('seasons.show', $season);
    $trail->push('Create race', route('races.create', [$series->universe, $series, $season]));
});

Breadcrumbs::for('races.edit', function ($trail, Race $race) {
    $season = $race->season;
    $series = $season->series;
    $trail->parent('seasons.show', $season);
    $trail->push("Edit {$race->name}", route('races.edit', [$series->universe, $series, $season, $race]));
});

Breadcrumbs::for('seasons.points', function ($trail, Season $season) {
    $series = $season->series;
    $trail->parent('seasons.show', $season);
    $trail->push("{$season->name} points system", route('seasons.points', [$series->universe, $series, $season]));
});

Breadcrumbs::for('seasons.reliability', function ($trail, Season $season) {
    $series = $season->series;
    $trail->parent('seasons.show', $season);
    $trail->push("{$season->name} reliability settings", route('seasons.points', [$series->universe, $series, $season]));
});

Breadcrumbs::for('seasons.qualifying', function ($trail, Season $season) {
	$series = $season->series;
	$trail->parent('seasons.show', $season);
	$trail->push("{$season->name} qualifying settings", route('seasons.qualifying', [$series->universe, $series, $season]));
});

Breadcrumbs::for('seasons.lineups.create', function ($trail, Season $season) {
    $series = $season->series;
    $trail->parent('seasons.show', $season);
    $trail->push("Add team to season", route('seasons.lineups.create', [$series->universe, $series, $season]));
});

Breadcrumbs::for('seasons.lineups.edit', function ($trail, Season $season, SeasonTeam $team) {
	$series = $season->series;
	$trail->parent('seasons.show', $season);
	$trail->push("Edit lineup", route('seasons.lineups.edit', [$series->universe, $series, $season, $team]));
});