<?php

use Illuminate\Support\Facades\Route;

Route::get('{series_id}/teams', 'TeamController@all')->name('teams.all');
Route::get('{universe_id}/{season_id}/drivers', 'DriverController@all')->name('drivers.all');
Route::get('{series_id}/engines', 'EngineController@all')->name('engines.all');

Route::get('lineups/{lineup_id}/find', 'LineupController@find')->name('lineups.find');

Route::get('{season_id}/lineups/get', 'LineupController@get')->name('lineups.get');
Route::post('{season_id}/lineups/store', 'LineupController@store')->name('lineups.store');
Route::patch('{season_id}/lineups/update', 'LineupController@update')->name('lineups.update');

Route::delete('{universe}/{series}/{season}/{lineup_id}/delete', 'LineupController@delete')->name('lineups.delete');