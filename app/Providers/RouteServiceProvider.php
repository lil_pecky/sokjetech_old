<?php

namespace App\Providers;

use App\Models\Season;
use App\Models\Series;
use App\Models\Universe;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * @var string
     */
    public const HOME = '/';

    /**
     * @var string
     */
    public const LOGIN = '/login';
    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        Route::bind('universe', function ($slug) {
            return Universe::whereSlug($slug)->firstOrFail();
        });

        Route::bind('series', function ($slug, $route) {
            return Series::whereSlug($slug)->where('universe_id', $route->parameter('universe')->id)->firstOrFail();
        });

        Route::bind('season', function ($year, $route) {
            return Season::where('series_id', $route->parameter('series')->id)
                ->where('year', $year)
                ->firstOrFail();
        });

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();

        $this->mapWebRoutes();

        //
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
            ->namespace($this->namespace)
            ->group(base_path('routes/web.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
            ->middleware('web')
            ->namespace($this->namespace)
            ->group(base_path('routes/api.php'));
    }
}
