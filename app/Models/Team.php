<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Team extends Model
{
    protected $fillable = [
        'name', 'team_principal', 'background_colour', 'text_colour', 'border_colour'
    ];

    protected $appends = ['styleString'];

    /**
     * @return string
     */
    public function getStyleStringAttribute(): string
    {
        $attribute = "background-color:{$this->background_colour};";
        $attribute .= "color:{$this->text_colour};border: 2px solid {$this->border_colour}";

        return $attribute;
    }

    /**
     * @return BelongsTo
     */
    public function series(): BelongsTo
    {
        return $this->belongsTo(Series::class);
    }
}
