<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class SeasonTeam extends Model
{
    protected $fillable = [
        'name',
        'team_principal',
        'background_colour',
        'text_colour',
        'border_colour',
        'rating',
        'reliability',
    ];

    protected $with = [
		'team', 'drivers', 'engine'
	];

    protected $hidden = [
    	'created_at', 'updated_at'
	];

	protected $appends = ['styleString'];

	/**
	 * @return string
	 */
	public function getStyleStringAttribute(): string
	{
		$attribute = "background-color:{$this->background_colour};";
		$attribute .= "color:{$this->text_colour};border: 2px solid {$this->border_colour}";

		return $attribute;
	}

    /**
     * @return BelongsTo
     */
    public function season(): BelongsTo
    {
        return $this->belongsTo(Season::class);
    }

    /**
     * @return BelongsTo
     */
    public function team(): BelongsTo
    {
        return $this->belongsTo(Team::class);
    }

	/**
	 * @return HasMany
	 */
    public function drivers(): HasMany
	{
		return $this->hasMany(DriverTeam::class, 'team_id');
	}

	/**
	 * @return HasOne
	 */
	public function engine(): HasOne
	{
		return $this->hasOne(EngineTeam::class, 'team_id');
	}
}
