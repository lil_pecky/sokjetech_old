<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Season extends Model
{
    protected $fillable = [
        'year', 'name', 'started', 'completed'
    ];

    protected $with = [
        'pointSystem', 'reliability', 'qualifyingSettings'
    ];

    /**
     * @return string
     */
    public function getRouteKeyName(): string
    {
        return 'year';
    }

    /**
     * @return BelongsTo
     */
    public function series(): BelongsTo
    {
        return $this->belongsTo(Series::class);
    }

    /**
     * @return HasMany
     */
    public function races(): HasMany
    {
        return $this->hasMany(Race::class);
    }

    /**
     * @return HasOne
     */
    public function pointSystem(): HasOne
    {
        return $this->hasOne(PointSystem::class);
    }

    /**
     * @return HasOne
     */
    public function reliability(): HasOne
    {
        return $this->hasOne(SeasonReliability::class);
    }

	/**
	 * @return HasOne
	 */
    public function qualifyingSettings(): HasOne
	{
		return $this->hasOne(QualifyingSettings::class);
	}

    /**
     * @return HasMany
     */
    public function teams(): HasMany
    {
        return $this->hasMany(SeasonTeam::class);
    }

    /**
     * @return HasMany
     */
    public function drivers(): HasMany
    {
        return $this->hasMany(Driver::class);
    }

    /**
     * @return HasMany
     */
    public function engines(): HasMany
    {
        return $this->hasMany(Engine::class);
    }
}
