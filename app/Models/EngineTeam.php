<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class EngineTeam extends Model
{
	protected $fillable = [
		'rating',
		'reliability',
		'name',
		'rebadge'
	];

	protected $with = ['engine'];

	protected $hidden = [
		'created_at', 'updated_at'
	];

	protected $appends = [
		'engine_name'
	];

	/**
	 * @return string
	 */
	public function getEngineNameAttribute(): string
	{
		return $this->rebadge ? $this->name : $this->engine->name;
	}

    /**
     * @return BelongsTo
     */
    public function engine(): BelongsTo
    {
        return $this->belongsTo(Engine::class);
    }

    /**
     * @return BelongsTo
     */
    public function season(): BelongsTo
    {
        return $this->belongsTo(Season::class);
    }

    /**
     * @return BelongsTo
     */
    public function team(): BelongsTo
    {
        return $this->belongsTo(SeasonTeam::class);
    }
}
