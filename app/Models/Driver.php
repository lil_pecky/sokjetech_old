<?php

namespace App\Models;

use DateTime;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Driver extends Model
{
    protected $fillable = [
        'first_name', 'last_name', 'birthdate'
    ];

    protected $appends = ['formattedBirthdate', 'full_name'];

    /**
     * @return BelongsTo
     */
    public function universe(): BelongsTo
    {
        return $this->belongsTo(Universe::class);
    }

	/**
	 * @return BelongsToMany
	 */
    public function seasons(): BelongsToMany
	{
		return $this->belongsToMany(Season::class, 'driver_teams', 'driver_id', 'season_id');
	}

    /**
     * @return string
     */
    public function getFullName(): string
    {
        return "{$this->first_name} {$this->last_name}";
    }

    public function getFullNameAttribute(): string
    {
        return $this->getFullName();
    }

    public function getFormattedBirthdateAttribute()
    {
        $dateTime = DateTime::createFromFormat('Y-m-d', $this->birthdate);
        return $dateTime->format('m/d/Y');
    }
}
