<?php

namespace App\Services;

use App\Models\Driver;
use App\Models\DriverTeam;
use App\Models\Engine;
use App\Models\EngineTeam;
use App\Models\Season;
use App\Models\SeasonTeam;
use App\Models\Team;
use Exception;

class LineupService
{
	private Season $season;

	private array $teamDetails;

	private array $drivers;

	private array $engine;

	private Team $baseTeam;

	private SeasonTeam $lineup;

	/**
	 * @param Season $season
	 * @param array $teamDetails
	 * @param array $drivers
	 * @param array $engine
	 * @param int $baseTeamId
	 * @param int|null $lineupId
	 */
	public function __construct(
		Season $season,
		array $teamDetails,
		array $drivers,
		array $engine,
		int $baseTeamId,
		?int $lineupId = null
	) {
		$this->season = $season;
		$this->teamDetails = $teamDetails;
		$this->drivers = $drivers;
		$this->engine = $engine;

		$this->baseTeam = Team::find($baseTeamId);

		if ($lineupId) {
			$this->lineup = SeasonTeam::find($lineupId);
		}
	}

	/**
	 * @throws Exception
	 */
	public function store()
	{
		$this->lineup = new SeasonTeam;
		$this->setLineupData();

		$this->lineup->team()->associate($this->baseTeam);
		$this->lineup->season()->associate($this->season);
		$this->lineup->save();

		$this->attachDrivers();
		$this->attachEngine();

		$this->lineup->save();
	}

	/**
	 * @throws Exception
	 */
	public function update()
	{
		$this->setLineupData();

		foreach ($this->lineup->drivers as $driver) {
			$driver->delete();
		}

		$this->attachDrivers();

		$this->lineup->engine()->delete();
		$this->attachEngine();

		$this->lineup->team()->dissociate();
		$this->lineup->team()->associate($this->baseTeam);

		$this->lineup->save();
	}

	/**
	 * @param int $lineupId
	 * @return array
	 */
	public static function find(int $lineupId): array
	{
		$ret = [];
		$drivers = [];

		$seasonTeam = SeasonTeam::find($lineupId);

		if (!$seasonTeam) {
			return [
				'success' => false,
				'error' => "Lineup doesn't exist"
			];
		}

		$ret['base_team'] = $seasonTeam->team->id;

		$ret['team_details'] = [
			'name' => $seasonTeam->name,
			'team_principal' => $seasonTeam->team_principal,
			'background_colour' => $seasonTeam->background_colour,
			'text_colour' => $seasonTeam->text_colour,
			'border_colour' => $seasonTeam->border_colour
		];

		foreach ($seasonTeam->drivers as $driver) {
			$drivers[] = [
				'id' => $driver->driver->id,
				'number' => $driver->number,
				'full_name' => $driver->driver->full_name
			];
		}

		$engine = [
			'name' => $seasonTeam->engine->name,
			'rebadge' => $seasonTeam->engine->rebadge,
			'engine' => $seasonTeam->engine->engine->toArray()
		];

		$ret['drivers'] = $drivers;
		$ret['engine'] = $engine;

		return $ret;
	}

	/**
	 * @param int $lineupId
	 * @return array
	 */
	public static function delete(int $lineupId): array
	{
		// TODO don't delete team after the season has started
		$lineup = SeasonTeam::find($lineupId);

		if (!$lineupId) {
			return [
				'success' => false,
				'error' => "Lineup doesn't exist"
			];
		}

		$lineup->team()->dissociate();
		$lineup->engine()->delete();

		foreach ($lineup->drivers as $driver) {
			$driver->delete();
		}

		$lineup->delete();

		return ['success' => true];
	}

	private function setLineupData()
	{
		$this->lineup->name = $this->teamDetails['name'];
		$this->lineup->team_principal = $this->teamDetails['team_principal'];
		$this->lineup->background_colour = $this->teamDetails['background_colour'];
		$this->lineup->text_colour = $this->teamDetails['text_colour'];
		$this->lineup->border_colour = $this->teamDetails['border_colour'];
	}

	/**
	 * @throws Exception
	 */
	private function attachEngine()
	{
		if (isset($this->engine['engine'])) {
			$baseEngine = $this->engine['engine'];
			$dbEngine = Engine::find($baseEngine['id']);

			if (!$dbEngine) {
				throw new Exception("Engine not found");
			}

			// If rebadged, use rebadged name, otherwise set null and use base engine name
			$engineName = $this->engine['rebadge'] ? $this->engine['name'] : null;

			$engineTeam = EngineTeam::create([
				'name' => $engineName,
				'rebadge' => $this->engine['rebadge']
			]);

			$engineTeam->engine()->associate($dbEngine);
			$engineTeam->team()->associate($this->lineup);
			$engineTeam->season()->associate($this->season);
			$engineTeam->save();
		}
	}

	/**
	 * @throws Exception
	 */
	private function attachDrivers()
	{
		foreach ($this->drivers as $driver) {
			$dbDriver = Driver::find($driver['id']);

			if (!$dbDriver) {
				throw new Exception("Driver not found");
			}

			$driverTeam = DriverTeam::create(['number' => (int) $driver['number']]);

			$driverTeam->driver()->associate($dbDriver);
			$driverTeam->team()->associate($this->lineup);
			$driverTeam->season()->associate($this->season);
			$driverTeam->save();
		}
	}
}