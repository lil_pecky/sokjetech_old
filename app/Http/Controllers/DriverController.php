<?php

namespace App\Http\Controllers;

use App\Http\Requests\NewDriverRequest;
use App\Http\Requests\UpdateDriverRequest;
use App\Models\Driver;
use App\Models\Season;
use App\Models\Universe;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class DriverController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * @param Universe $universe
     * @return Renderable
     */
    public function create(Universe $universe): Renderable
    {
        return view('drivers.create', [
            'universe' => $universe
        ]);
    }

    /**
     * @param NewDriverRequest $request
     * @param Universe $universe
     * @return RedirectResponse
     */
    public function store(NewDriverRequest $request, Universe $universe): RedirectResponse
    {
        $driver = Driver::create([
            'first_name' => $request->get('first_name'),
            'last_name' => $request->get('last_name'),
            'birthdate' => $request->get('birthdate')
        ]);

        $driver->universe()->associate($universe);
        $driver->save();

        return redirect()->route('universes.show', $universe)->with('tab', 'drivers');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * @param Universe $universe
     * @param Driver $driver
     * @return Renderable
     */
    public function edit(Universe $universe, Driver $driver): Renderable
    {
        return view('drivers.edit', [
            'driver' => $driver
        ]);
    }

    /**
     * @param NewDriverRequest $request
     * @param Universe $universe
     * @param Driver $driver
     * @return RedirectResponse
     */
    public function update(NewDriverRequest $request, Universe $universe, Driver $driver): RedirectResponse
    {
        if ($universe->id === $driver->universe_id) {
            $driver->first_name = $request->get('first_name');
            $driver->last_name = $request->get('last_name');
            $driver->birthdate = $request->get('birthdate');
            $driver->save();
        }

        return redirect()->route('universes.show', $universe)->with('tab', 'drivers');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * @param int $universe
	 * @param int $seasonId
     * @return Collection
     */
    public function all(int $universe, int $seasonId): Collection
    {
		$availableDrivers = DB::select("SELECT id FROM drivers WHERE id NOT IN
									(SELECT driver_id FROM driver_teams WHERE season_id = ?)
									AND universe_id = ?", [$seasonId, $universe]);
		$drivers = new Collection();

		foreach ($availableDrivers as $driver) {
			$drivers->add(Driver::find($driver->id));
		}

		return $drivers;
    }
}
