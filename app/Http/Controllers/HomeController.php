<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @return Renderable
     */
    public function index(): Renderable
    {
        return view('index');
    }

    /**
     * @return Renderable
     */
    public function sim(): Renderable
    {
        return view('sim');
    }
}
