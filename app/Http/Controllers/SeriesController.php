<?php

namespace App\Http\Controllers;

use App\Http\Requests\NewSeriesRequest;
use App\Http\Requests\UpdateSeriesRequest;
use App\Models\Series;
use App\Models\Universe;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SeriesController extends Controller
{
    /**
     * @param Universe $universe
     * @param Series $series
     * @return Renderable
     */
    public function index(Universe $universe, Series $series): Renderable
    {
        return view('series.index', [
            'series' => $series
        ]);
    }

    /**
     * @param Universe $universe
     * @return Renderable
     */
    public function create(Universe $universe): Renderable
    {
        return view('series.create', [
            'universe' => $universe
        ]);
    }

    /**
     * @param NewSeriesRequest $request
     * @param Universe $universe
     * @return RedirectResponse
     */
    public function store(NewSeriesRequest $request, Universe $universe): RedirectResponse
    {
        $series = Series::create([
            'name' => $request->get('name'),
            'slug' => $request->get('slug')
        ]);

        $series->universe()->associate($universe);
        $series->user()->associate(Auth::user());

        $series->save();

        return redirect()->route('series.show', [
            'universe' => $universe,
            'series' => $series
        ]);
    }

    /**
     * @param Request $request
     * @param Universe $universe
     * @param Series $series
     * @return Renderable
     */
    public function show(Request $request, Universe $universe, Series $series): Renderable
    {
        // TODO see if this can be moved to a central place for DRY reasons (constructor?)
        $tab = $request->session()->get('tab') ?? 'seasons';
        $canEdit = Auth::check() && Auth::user()->ownsUniverse($universe);
        $canSee = $canEdit || $universe->public;
        $series->load('teams', 'engines', 'seasons');

        return view('series.show', [
            'series' => $series,
            'tab' => $tab,
            'canEdit'=> $canEdit,
            'canSee' => $canSee
        ]);
    }

    /**
     * @param Universe $universe
     * @param Series $series
     * @return Renderable
     */
    public function edit(Universe $universe, Series $series): Renderable
    {
        return view('series.edit', [
            'series' => $series
        ]);
    }

    /**
     * @param UpdateSeriesRequest $request
     * @param Universe $universe
     * @param Series $series
     * @return RedirectResponse
     */
    public function update(UpdateSeriesRequest $request, Universe $universe, Series $series): RedirectResponse
    {
        $series->name = $request->get('name');
        $series->slug = $request->get('slug');

        $series->save();

        return redirect()->route('universes.show', [
            'universe' => $universe
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Checks if the generated slug already exists for series
     *
     * @param string $slug
     * @return bool true if slug exists within universe, false if not
     */
    public function duplicateSlug(Request $request): bool
    {
        $slug = $request->get('slug');

        $duplicate = Series::where('slug', $slug)->where('universe_id', (int) $request->get('universe_id'));

        if ($request->get('series_id') !== null) {
            $duplicate->where('id', '!=', (int) $request->get('series_id'));
        }

        return $duplicate->count() > 0 ? true : false;
    }
}
