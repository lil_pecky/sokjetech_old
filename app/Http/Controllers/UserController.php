<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateUserRequest;
use App\Models\User;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\RedirectResponse;

class UserController extends Controller
{
    /**
     * @param  User $user
     * @return Renderable
     */
    public function show(User $user): Renderable
    {
        return view('users.show', [
             'user' => $user
        ]);
    }

    /**
     * @param  User $user
     * @return Renderable
     */
    public function edit(User $user): Renderable
    {
        return view('users.edit', [
            'user' => $user
        ]);
    }

    /**
     * @param UpdateUserRequest $request
     * @param User $user
     * @return RedirectResponse
     */
    public function update(UpdateUserRequest $request, User $user): RedirectResponse
    {
        $user->username = $request->get('username');
        $user->email = $request->get('email');
        $user->discord = $request->get('discord');
        $user->save();

        $request->session()->flash('message', 'User information updated.');

        return redirect()->route('users.show', $user);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
