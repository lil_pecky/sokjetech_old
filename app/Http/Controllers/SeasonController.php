<?php

namespace App\Http\Controllers;

use App\Http\Requests\NewSeasonRequest;
use App\Models\PointSystem;
use App\Models\QualifyingSettings;
use App\Models\Season;
use App\Models\SeasonReliability;
use App\Models\Series;
use App\Models\Universe;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SeasonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function create(Universe $universe, Series $series): Renderable
    {
        return view('seasons.create', [
            'series' => $series
        ]);
    }

    /**
     * @param NewSeasonRequest $request
     * @param Universe $universe
     * @param Series $series
     * @return RedirectResponse
     */
    public function store(NewSeasonRequest $request, Universe $universe, Series $series): RedirectResponse
    {
        $year = $request->get('year');
        $name = $request->get('name');

        if (!$name) {
            $name = "{$year} {$series->name} season";
        }

        $season = Season::create([
            'year' => $year,
            'name' => $name,
            'started' => false,
            'completed' => false
        ]);

        $season->series()->associate($series);
        $season->save();

        return redirect()->route('series.show', [
            'universe' => $universe,
            'series' => $series
        ])->with('tab', 'seasons');
    }

    /**
     * @param Request $request
     * @param Universe $universe
     * @param Series $series
     * @param Season $season
     * @return Renderable
     */
    public function show(Request $request, Universe $universe, Series $series, Season $season, ?string $tab = null): Renderable
    {
        $season->load('races');

        // TODO improve this logic
//        $tab = $request->session()->get('tab') ?? $request->get('tab');
//        $tab = $tab ?? 'races';
        if (!$tab) {
            if ($value = $request->session()->get('tab')) {
                $tab = $value;
            } else {
                $tab = 'races';
            }
        }

        $canEdit = Auth::check() && Auth::user()->ownsUniverse($universe);

        return view('seasons.show', [
            'season' => $season,
            'tab' => $tab,
            'canEdit' => $canEdit
        ]);
    }

    /**
     * @param Universe $universe
     * @param Series $series
     * @param Season $season
     * @return Renderable
     */
    public function edit(Universe $universe, Series $series, Season $season): Renderable
    {
        return view('seasons.edit', [
            'universe' => $universe,
            'series' => $series,
            'season' => $season
        ]);
    }

    /**
     * @param NewSeasonRequest $request
     * @param Universe $universe
     * @param Series $series
     * @param Season $season
     * @return RedirectResponse
     */
    public function update(NewSeasonRequest $request, Universe $universe, Series $series, Season $season): RedirectResponse
    {
        $season->year = $request->get('year');
        $season->name = $request->get('name');
        $season->save();

        return redirect()->route('series.show', [
            'universe' => $universe,
            'series' => $series
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * @param Universe $universe
     * @param Series $series
     * @param Season $season
     * @return Renderable
     */
    public function points(Universe $universe, Series $series, Season $season): Renderable
    {
        $season->load('pointSystem');
        return view('seasons.points', [
            'universe' => $universe,
            'series' => $series,
            'season' => $season
        ]);
    }

    /**
     * @param Request $request
     * @param Universe $universe
     * @param Series $series
     * @param Season $season
     * @return array
     */
    public function storePoints(Request $request, Universe $universe, Series $series, Season $season): array
    {
        $points = $this->parsePoints($request->get('points'));
        if (!$season->pointSystem) {
            $pointsSystem = PointSystem::create([
                'points' => $points
            ]);

            $pointsSystem->season()->associate($season);
        } else {
            $pointsSystem = $season->pointSystem;
            $pointsSystem->points = $points;
        }

        $pointsSystem->pole_points = $request->get('polePoints');
        $pointsSystem->fastest_lap_points = $request->get('fastestLapPoints');

        $pointsSystem->save();

        return [
            'success' => true
        ];
    }

    public function reliability(Universe $universe, Series $series, Season $season): Renderable
    {
        $season->load('reliability');
        return view('seasons.reliability', [
            'universe' => $universe,
            'series' => $series,
            'season' => $season
        ]);
    }

    /**
     * @param Request $request
     * @param Universe $universe
     * @param Series $series
     * @param Season $season
     * @return array
     */
    public function storeReliability(Request $request, Universe $universe, Series $series, Season $season): array
    {
        $settings = json_encode($request->get('types'));

        if (!$season->reliability) {
            $reliability = SeasonReliability::create([
                'settings' => $settings
            ]);

            $reliability->season()->associate($season);
        } else {
            $reliability = $season->reliability;
            $reliability->settings = $settings;
        }
        $reliability->save();

        return [
            'success' => true
        ];
    }

	/**
	 * @param Universe $universe
	 * @param Series $series
	 * @param Season $season
	 * @return Renderable
	 */
    public function qualifying(Universe $universe, Series $series, Season $season): Renderable
	{
		return view('seasons.qualifying', [
			'universe' => $universe,
			'series' => $series,
			'season' => $season
		]);
	}

	/**
	 * @param Request $request
	 * @param Universe $universe
	 * @param Series $series
	 * @param Season $season
	 * @return bool[]
	 */
	public function storeQualifying(Request $request, Universe $universe, Series $series, Season $season): array
	{
		$settings = json_encode($request->get('settings'));
		if (!$season->qualifyingSettings) {
			$qualifyingSettings = QualifyingSettings::create([
				'type' => $request->get('type'),
				'settings' => $settings
			]);

			$qualifyingSettings->season()->associate($season);
		} else {
			$qualifyingSettings = $season->qualifyingSettings;
			$qualifyingSettings->settings = $settings;
		}

		$qualifyingSettings->save();

		return ['success' => true];
	}

	/**
	 * @param Universe $universe
	 * @param Series $series
	 * @param Season $season
	 * @return array
	 */
	public function start(Universe $universe, Series $series, Season $season): array
	{
		$season->started = true;
		$season->save();

		return ['success' => true];
	}

    /**
     * TODO move to service
     *
     * @param array $points
     * @return string
     */
    private function parsePoints(array $points): string
    {
        foreach ($points as $key => $point) {
            $points[$key]['points'] = (int) $point['points'];
        }

        return json_encode($points);
    }

    private function getRollRange(array $setting): string
    {
        return json_encode([
            'min' => (int) $setting['min'],
            'max' => (int) $setting['max']
        ]);
    }
}
