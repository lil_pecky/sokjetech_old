<?php

namespace App\Http\Controllers;

use App\Models\Season;
use App\Models\SeasonTeam;
use App\Models\Series;
use App\Models\Universe;
use App\Services\LineupService;
use Exception;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class LineupController extends Controller
{
	/**
	 * @param Universe $universe
	 * @param Series $series
	 * @param Season $season
	 * @return Renderable
	 */
	public function create(Universe $universe, Series $series, Season $season): Renderable
	{
		return view('seasons.lineups.create', [
			'season' => $season
		]);
	}

	/**
	 * @param Universe $universe
	 * @param Series $series
	 * @param Season $season
	 * @param SeasonTeam $lineup
	 * @return Renderable
	 */
	public function edit(
		Universe $universe,
		Series $series,
		Season $season,
		SeasonTeam $lineup
	): Renderable {
		return view('seasons.lineups.edit', [
			'season' => $season,
			'lineup' => $lineup
		]);
	}

	/**
	 * @param Request $request
	 * @param int $season
	 * @return array
	 * @throws Exception
	 */
    public function store(Request $request, int $season): array
	{
		$lineupService = new LineupService(
			Season::find($season),
			$request->get('team_details'),
			$request->get('drivers'),
			$request->get('engine'),
			$request->get('base_team')
		);

		$lineupService->store();

		return [
			'success' => true
		];
	}

	/**
	 * @param Request $request
	 * @param int $season
	 * @return array
	 * @throws Exception
	 */
	public function update(Request $request, int $season): array
	{
		$lineupService = new LineupService(
			Season::find($season),
			$request->get('team_details'),
			$request->get('drivers'),
			$request->get('engine'),
			$request->get('base_team'),
			$request->get('lineup_id')
		);

		$lineupService->update();

		return ['success' => true];
	}

	/**
	 * @param int $seasonId
	 * @return Collection
	 * @throws Exception
	 */
	public function get(int $seasonId): Collection
	{
		$season = Season::find($seasonId);

		if (!$season) {
			throw new Exception("Season not found");
		}

		return $season->teams;
	}

	/**
	 * @param int $lineupId
	 * @return array
	 */
	public function find(int $lineupId): array
	{
		return LineupService::find($lineupId);
	}

	/**
	 * @param Universe $universe
	 * @param Series $series
	 * @param Season $season
	 * @param int $lineupId
	 * @return array
	 */
	public function delete(Universe $universe, Series $series, Season $season, int $lineupId): array
	{
		return LineupService::delete($lineupId);
	}
}
