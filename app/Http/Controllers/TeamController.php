<?php

namespace App\Http\Controllers;

use App\Http\Requests\NewTeamRequest;
use App\Models\Series;
use App\Models\Team;
use App\Models\Universe;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class TeamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * @param Universe $universe
     * @param Series $series
     * @return Renderable
     */
    public function create(Universe $universe, Series $series): Renderable
    {
        return view('teams.create', [
            'universe' => $universe,
            'series' => $series
        ]);
    }

    /**
     * @param NewTeamRequest $request
     * @param Universe $universe
     * @param Series $series
     * @return RedirectResponse
     */
    public function store(NewTeamRequest $request, Universe $universe, Series $series): RedirectResponse
    {
        $team = Team::create([
            'name' => $request->get('name'),
            'team_principal' => $request->get('team_principal'),
            'background_colour' => $request->get('background_colour'),
            'text_colour' => $request->get('text_colour'),
            'border_colour' => $request->get('border_colour')
        ]);

        $team->series()->associate($series);
        $team->save();

        return redirect()->route('series.show', [$universe, $series])->with('tab', 'teams');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * @param Universe $universe
     * @param Series $series
     * @param Team $team
     * @return Renderable
     */
    public function edit(Universe $universe, Series $series, Team $team): Renderable
    {
        return view('teams.edit', [
            'universe' => $universe,
            'series'=> $series,
            'team' => $team
        ]);
    }

    /**
     * @param NewTeamRequest $request
     * @param Universe $universe
     * @param Series $series
     * @param Team $team
     * @return RedirectResponse
     */
    public function update(NewTeamRequest $request, Universe $universe, Series $series, Team $team): RedirectResponse
    {
        $team->name = $request->get('name');
        $team->team_principal = $request->get('team_principal');
        $team->background_colour = $request->get('background_colour');
        $team->text_colour = $request->get('text_colour');
        $team->border_colour = $request->get('border_colour');

        $team->save();

        return redirect()->route('series.show', [$universe, $series])->with('tab', 'teams');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * @param int $series
     * @return Collection
     */
    public function all(int $series): Collection
    {
        return Team::where('series_id', $series)->get();
    }
}
