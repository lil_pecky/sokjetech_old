<?php

namespace App\Http\Controllers;

use App\Http\Requests\NewUniverseRequest;
use App\Http\Requests\UpdateUniverseRequest;
use App\Models\Driver;
use App\Models\Universe;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UniverseController extends Controller
{
    /**
     * @return Renderable
     */
    public function index(): Renderable
    {
        $universes = Universe::with('user')->where('public', true);

        if (Auth::check()) {
            $universes->orWhere('user_id', Auth::user()->id);
        }

        return view('universes.index', [
            'universes' => $universes->get()
        ]);
    }

    /**
     * @return Renderable
     */
    public function create(): Renderable
    {
        return view('universes.create');
    }

    /**
     * @param NewUniverseRequest $request
     * @return RedirectResponse
     */
    public function store(NewUniverseRequest $request): RedirectResponse
    {
        $public = $request->get('public') !== null;
        $universe = Universe::create([
            'name' => $request->get('name'),
            'slug' => $request->get('slug'),
            'public' => $public
        ]);

        $user = Auth::user();

        $universe->user()->associate($user);
        $universe->save();

        return redirect()->route('universes.index');
    }

    /**
     * @param Request $request
     * @param Universe $universe
     * @return Renderable
     */
    public function show(Request $request, Universe $universe): Renderable
    {
        $tab = $request->session()->get('tab') ?? 'series';
        $universe->load('series', 'drivers');
        $canEdit = Auth::check() && Auth::user()->ownsUniverse($universe);

        return view('universes.show', [
            'universe' => $universe,
            'canEdit' => $canEdit,
            'tab' => $tab
        ]);
    }

    /**
     * @param  Universe $universe
     * @return Renderable
     */
    public function edit(Universe $universe): Renderable
    {
        return view('universes.edit', [
            'universe' => $universe
        ]);
    }

    /**
     * @param UpdateUniverseRequest $request
     * @param Universe $universe
     * @return RedirectResponse
     */
    public function update(UpdateUniverseRequest $request, Universe $universe): RedirectResponse
    {
        $public = $request->get('public') !== null;

        $universe->name = $request->get('name');
        $universe->slug = $request->get('slug');
        $universe->public = $public;

        $universe->save();

        return redirect()->route('universes.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Checks if the generated slug already exists for universes
     *
     * @param string $slug
     * @return bool
     */
    public function duplicateSlug(Request $request): bool
    {
        $slug = $request->get('slug');

        $duplicate = Universe::where('slug', $slug);

        if ($request->get('universe_id') !== null) {
            $duplicate->where('id', '!=', (int) $request->get('universe_id'));
        }

        return $duplicate->count() > 0 ? true : false;
    }
}
