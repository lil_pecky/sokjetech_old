<?php

namespace App\Http\Controllers;

use App\Models\Race;
use App\Models\Season;
use App\Models\Series;
use App\Models\Universe;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RaceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * @param Universe $universe
     * @param Series $series
     * @param Season $season
     * @return Renderable
     */
    public function create(Universe $universe, Series $series, Season $season): Renderable
    {
        $canEdit = Auth::check() && Auth::user()->ownsUniverse($universe);

        return view('races.create', [
            'season' => $season,
            'canEdit' => $canEdit
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Universe $universe
     * @param Series $series
     * @param Season $season
     * @param Request $request
     * @return array
     */
    public function store(Universe $universe, Series $series, Season $season, Request $request): array
    {
        // TODO check if race already exists
        $name = $request->get('name');
        $circuit = $request->get('circuit');
        $country = $request->get('country');
        $stints = $this->parseStints($request->get('stints'));

        $race = Race::create([
            'name' => $name,
            'circuit' => $circuit,
            'country' => $country,
            'stints' => $stints
        ]);

        $race->season()->associate($season);
        $race->save();

        return [
            'success' => true
        ];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * @param Universe $universe
     * @param Series $series
     * @param Season $season
     * @param Race $race
     * @return Renderable
     */
    public function edit(Universe $universe, Series $series, Season $season, Race $race): Renderable
    {
        // TODO move to central spot so I don't have to keep repeating the same thing over and over again
        $canEdit = Auth::check() && Auth::user()->ownsUniverse($universe);

        return view('races.edit', [
            'universe' => $universe,
            'series' => $series,
            'season' => $season,
            'race' => $race,
            'canEdit' => $canEdit
        ]);
    }

    /**
     * @param Request $request
     * @param Universe $universe
     * @param Series $series
     * @param Season $season
     * @param Race $race
     * @return array
     */
    public function update(Request $request, Universe $universe, Series $series, Season $season, Race $race): array
    {
        $name = $request->get('name');
        $circuit = $request->get('circuit');
        $country = $request->get('country');
        $stints = $this->parseStints($request->get('stints'));

        $race->name = $name;
        $race->circuit = $circuit;
        $race->country = $country;
        $race->stints = $stints;
        $race->save();

        return [
            'success' => true
        ];
    }

    /**
     * @param Universe $universe
     * @param Series $series
     * @param Season $season
     * @param Race $race
     * @return array
     * @throws \Exception
     */
    public function destroy(Universe $universe, Series $series, Season $season, Race $race): array
    {
        if ($season->started || $season->completed) {
            return [
                'success' => false,
                'error' => "Season has already been started"
            ];
        }

        $race->delete();

        return [
            'success' => true
        ];
    }

    /**
     * @param array $stints
     * @return string
     */
    private function parseStints(array $stints): string
    {
        foreach ($stints as $key => $stint) {
            $stints[$key]['min'] = (int) $stint['min'];
            $stints[$key]['max'] = (int) $stint['max'];
        }

        return json_encode($stints);
    }
}
