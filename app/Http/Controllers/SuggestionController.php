<?php

namespace App\Http\Controllers;

use App\Http\Requests\NewSuggestionRequest;
use App\Http\Requests\UpdateSuggestionRequest;
use App\Models\Suggestion;
use App\Models\SuggestionCategory;
use App\Models\SuggestionStatus;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SuggestionController extends Controller
{
    /**
     * @return Renderable
     */
    public function index(): Renderable
    {
        $suggestions = Suggestion::all();

        return view('suggestions.index', [
            'suggestions' => $suggestions
        ]);
    }

    /**
     * @return Renderable
     */
    public function create(): Renderable
    {
        $categories = SuggestionCategory::orderBy('name', 'asc')->get();

        return view('suggestions.create', [
            'categories' => $categories
        ]);
    }

    /**
     * @param NewSuggestionRequest $request
     * @return RedirectResponse
     */
    public function store(NewSuggestionRequest $request): RedirectResponse
    {
        $suggestion = Suggestion::create([
            'title' => $request->get('title'),
            'description' => $request->get('description')
        ]);

        $category = SuggestionCategory::find((int) $request->get('category_id'));
        $status = SuggestionStatus::where('name', 'new')->first();

        $suggestion->category()->associate($category);
        $suggestion->status()->associate($status);
        $suggestion->user()->associate(Auth::user());

        $suggestion->save();

        return redirect()->route('suggestions.index');
    }

    /**
     * @param Suggestion $suggestion
     * @return Renderable
     */
    public function edit(Suggestion $suggestion): Renderable
    {
        $categories = SuggestionCategory::orderBy('name', 'asc')->get();

        return view('suggestions.edit', [
            'suggestion' => $suggestion,
            'categories' => $categories
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateSuggestionRequest $request
     * @param  Suggestion $suggestion
     * @return RedirectResponse
     */
    public function update(UpdateSuggestionRequest $request, Suggestion $suggestion): RedirectResponse
    {
        $suggestion->title = $request->get('title');
        $suggestion->description = $request->get('description');

        $category = SuggestionCategory::find((int) $request->get('category_id'));

        $suggestion->category()->associate($category);
        $suggestion->save();

        return redirect()->route('suggestions.index');
    }

    /**
     * Changes suggestion status
     *
     * @param Request $request
     * @param Suggestion $suggestion
     * @return RedirectResponse
     */
    public function status(Request $request, Suggestion $suggestion): RedirectResponse
    {
        $statusName = "{$request->get('action')}ed";
        $status = SuggestionStatus::where('name', $statusName)->first();

        $suggestion->status()->associate($status);
        $suggestion->save();

        $request->session()->flash('message', 'Status updated');
        return redirect()->route('suggestions.index');
    }
}
