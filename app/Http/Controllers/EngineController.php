<?php

namespace App\Http\Controllers;

use App\Http\Requests\NewEngineRequest;
use App\Models\Engine;
use App\Models\Series;
use App\Models\Universe;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class EngineController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * @param Universe $universe
     * @param Series $series
     * @return Renderable
     */
    public function create(Universe $universe, Series $series): Renderable
    {
        return view('engines.create', [
            'universe' => $universe,
            'series' => $series
        ]);
    }

    /**
     * @param NewEngineRequest $request
     * @param Universe $universe
     * @param Series $series
     * @return RedirectResponse
     */
    public function store(NewEngineRequest $request, Universe $universe, Series $series): RedirectResponse
    {
        $engine = Engine::create([
            'name' => $request->get('name')
        ]);

        $engine->series()->associate($series);
        $engine->save();

        return redirect()->route('series.show', [$universe, $series])->with('tab', 'engines');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * @param Universe $universe
     * @param Series $series
     * @param Engine $engine
     * @return Renderable
     */
    public function edit(Universe $universe, Series $series, Engine $engine): Renderable
    {
        return view('engines.edit', [
            'universe' => $universe,
            'engine' => $engine,
            'series' => $series
        ]);
    }

    /**
     * @param NewEngineRequest $request
     * @param Universe $universe
     * @param Series $series
     * @param Engine $engine
     * @return RedirectResponse
     */
    public function update(NewEngineRequest $request, Universe $universe, Series $series, Engine $engine): RedirectResponse
    {
        $engine->name = $request->get('name');
        $engine->save();

        return redirect()->route('series.show', [$universe, $series])->with('tab', 'engines');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * @param int $series
     * @return Collection
     */
    public function all(int $series): Collection
    {
        return Engine::where('series_id', $series)->get();
    }
}
