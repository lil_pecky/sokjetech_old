<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CanSeeUniverse
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $universe = $request->route('universe');

        if ($universe->public || (Auth::check() && Auth::user()->id === $universe->user_id)) {
            return $next($request);
        }
        return redirect()->route('universes.index');
    }
}
