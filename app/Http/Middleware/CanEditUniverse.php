<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CanEditUniverse
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $universe = $request->route('universe');

        if (Auth::check() && Auth::user()->id === $universe->user_id) {
            return $next($request);
        }
        return redirect()->route('universes.index');
    }
}
