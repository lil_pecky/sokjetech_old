<?php

namespace App\Http\Requests;

use App\Models\Universe;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class UpdateUniverseRequest extends FormRequest
{
    private Universe $universe;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $this->universe = $this->route('universe');

        return Auth::check() && Auth::user()->id === $this->universe->user_id;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'slug' => [
                'required',
                'max:255',
                Rule::unique('universes')->ignore($this->universe->id)
            ]
        ];
    }
}
