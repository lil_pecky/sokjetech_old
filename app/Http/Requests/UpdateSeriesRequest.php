<?php

namespace App\Http\Requests;

use App\Models\Series;
use App\Models\Universe;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class UpdateSeriesRequest extends FormRequest
{
    private Series $series;
    private Universe $universe;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $this->universe = $this->route('universe');

        return Auth::check() && Auth::user()->id === $this->universe->user_id;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $this->series = $this->route('series');
        return [
            'name' => 'required|max:255',
            'slug' => [
                'required',
                'max:255',
                // Rule::unique('series')->ignore($this->series->id)
            ]
        ];
    }
}
