<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class NewTeamRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $universe = $this->route('universe');

        return Auth::check() && Auth::user()->ownsUniverse($universe);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'team_principal' => 'required|max:255',
            'background_colour' => 'required|max:8',
            'text_colour' => 'required|max:8',
            'border_colour' => 'required|max:8'
        ];
    }
}
